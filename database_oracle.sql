DROP TABLE order_item;
DROP TABLE main_order;
DROP TABLE person;
DROP TABLE product;

CREATE TABLE person(
	id_person INTEGER PRIMARY KEY,
	username VARCHAR2(30) UNIQUE,
	password VARCHAR2(30) NOT NULL,
	firstname VARCHAR2(30) NOT NULL,
	lastname VARCHAR2(30) NOT NULL,
	admin_rights INTEGER DEFAULT 0, 
	birthday DATE,
	phone_number VARCHAR2(12) NOT NULL,
	email VARCHAR2(50) NOT NULL
);

CREATE TABLE product(
	id INTEGER PRIMARY KEY,
	name VARCHAR2(30) NOT NULL,
	carbo INTEGER DEFAULT 1,
	stock INTEGER NOT NULL,
	flavour VARCHAR2(20) NOT NULL,
	img_path VARCHAR2(40) NOT NULL,
	available INTEGER DEFAULT 1,
	price NUMBER NOT NULL,
	volume NUMBER NOT NULL,
	date_added DATE DEFAULT SYSDATE
);

CREATE TABLE main_order(
	id INTEGER PRIMARY KEY,
	idPerson INTEGER NOT NULL,
	date_added DATE DEFAULT SYSDATE,
	FOREIGN KEY (idPerson) REFERENCES person(id_person) ON DELETE CASCADE
);

CREATE TABLE order_item(
	order_id INTEGER NOT NULL,
	product_id NUMBER NOT NULL,
	price NUMBER NOT NULL,
	quantity INTEGER NOT NULL,
	FOREIGN KEY (order_id) REFERENCES main_order(id) ON DELETE CASCADE,
	FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE
);

DROP SEQUENCE product_seq;
CREATE SEQUENCE product_seq;

DROP SEQUENCE personID_generator;
CREATE SEQUENCE personID_generator
	MINVALUE 10000
	START WITH 10000
	INCREMENT BY 1
	CACHE 20;

DROP SEQUENCE order_seq;
CREATE SEQUENCE order_seq
	MINVALUE 1
	START WITH 1
	INCREMENT BY 10
	CACHE 20;
	
CREATE OR REPLACE TRIGGER idPerson
BEFORE INSERT ON person
FOR EACH ROW
BEGIN
	SELECT personID_generator.NEXTVAL
	INTO :new.id_person
	FROM dual;
END;
/
CREATE OR REPLACE TRIGGER idProduct 
BEFORE INSERT ON product
FOR EACH ROW
BEGIN
	SELECT product_seq.NEXTVAL
	INTO :new.id
	FROM dual;
END;
/

/
INSERT INTO person(username, password, firstname, lastname, birthday, phone_number, email) VALUES ('deliutza94', 'fetitzatha', 'Delia', 'Dila', TO_DATE('1994-5-17', 'YYYY-MM-DD'), '0762112112', 'delia.dila@info.uaic.ro');
INSERT INTO person (username, password, firstname, lastname, phone_number, email) VALUES ('dumitru.poca', 'I suk @ Dotka', 'Dumitru', 'Poca','0762112113', 'dumitru.poca@info.uaic.ro');
INSERT INTO person (username, password, firstname, lastname, phone_number, email) VALUES ('daniel.redinciuc', 'FSMrules', 'Daniel', 'Redinciuc', '0762112114', 'ilie.hredniciuc@info.uaic.ro');
INSERT INTO person (username, password, firstname, lastname, admin_rights, phone_number, email) VALUES ('florin.purice', 'mafiotu#1', 'Florin', 'Purice', 1 , '0762112111', 'florin.purice@info.uaic.ro');

INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('CocaCola', 20, 'Cola', 'img/CocaCola.png', 5, 1.5, TO_DATE('2015-4-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Pepsi', 22, 'Cola', 'img/Pepsi.jpg', 3.5, 0.5, TO_DATE('2015-4-16', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Monster Energy', 30, 'Energizer', 'img/MonsterEnergy.gif', 6, 0.5, TO_DATE('2015-3-15', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('RedBull', 15, 'Energizer', 'img/RedBull.jpeg', 8, 0.5, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, carbo, flavour, stock, img_path, price, volume, date_added) VALUES ('Prigat', 0, 'Kiwi', 20, 'img/PrigatKiwi.jpg', 5.3, 1.25, TO_DATE('2015-3-17', 'YYYY-MM-DD'));
INSERT INTO product (name, carbo, flavour, stock, img_path, price, volume, date_added) VALUES ('Prigat', 0, 'Pear', 20, 'img/PrigatPear.jpg', 5.3, 1.25, TO_DATE('2015-5-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Fanta', 24, 'Orange', 'img/FantaOrange.jpg', 4.5, 2, TO_DATE('2015-4-12', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Fanta', 24, 'Grapes', 'img/FantaGrapes.jpg', 4.5, 2.5, TO_DATE('2015-2-13', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Fanta', 24, 'Rasberry', 'img/FantaRasberry.jpg', 4.5, 2.5, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Fanta', 24, 'Lemon', 'img/FantaLemon.jpg', 2, 0.5, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Fanta', 24, 'Apple', 'img/FantaAppleSourCherry.jpg', 4.5, 2, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Sunny Delight', 33, 'California','img/SunnyDelightCalifornia.jpg', 5.6, 1, TO_DATE('2015-3-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Sunny Delight', 33, 'Florida','img/SunnyDelightFlorida.jpg', 5.6, 1, TO_DATE('2015-3-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('DrPepper', 100, 'Cola', 'img/DrPepper.jpg', 3.1, 0.3, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Sprite', 54, 'Lemon', 'img/Sprite.jpg', 4, 2.5, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Mirinda', 42, 'Orange', 'img/MirindaOrange.jpg', 4, 2, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Mirinda', 42, 'Lemon', 'img/MirindaLemon.jpg', 4, 2, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('Mirinda', 42, 'Grapes', 'img/MirindaGrapes.jpg', 4, 2, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('RockStar', 65, 'Energizer', 'img/RockStar.jpg', 5.3, 0.5, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('MountainDew', 22, 'Energizer', 'img/MountainDew.jpg', 3.5, 1.25, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('7up', 52, 'Lemon', 'img/7upLemon.jpg', 3.7, 0.3, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, flavour, img_path, price, volume, date_added) VALUES ('7up', 52, 'Cherry', 'img/7upCherry.jpg', 3.7, 0.3, TO_DATE('2015-2-17', 'YYYY-MM-DD'));
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Cappy', 50, 0, 'Orange', 'img/CappyOrange.jpg', 7, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Cappy', 50, 0, 'Grapefruit', 'img/CappyGrapefruit.jpg', 7, 1.2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Cappy', 50, 0, 'Strawberry', 'img/CappyStrawberry.jpg', 7, 1.2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Cappy', 50, 0, 'Peach', 'img/CappyPeach.jpg', 7, 1.2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tymbark', 100, 0, 'Sour Cherry', 'img/TymbarkSourCherry.jpg', 4, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tymbark', 120, 0, 'Orange', 'img/TymbarkOrange.jpg', 4, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tymbark', 110, 0, 'Peach', 'img/TymbarkPeach.jpg', 4, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tymbark', 120, 0, 'Green Apple', 'img/TymbarkGreenApple.jpg', 4, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Ciao', 200, 0, 'Peach', 'img/CiaoPeach.jpg', 4.5, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Ciao', 200, 0, 'Apple', 'img/CiaoApple.jpg', 4.5, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Ciao', 200, 0, 'Strawberry', 'img/CiaoStrawberry.jpg', 4.5, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Ciao', 200, 0, 'Orange', 'img/CiaoOrange.jpg', 4.5, 2);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Orange', 'img/SantalOrange.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Pear', 'img/SantalPear.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Peach', 'img/SantalPeach.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Berry', 'img/SantalBerries.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Pomegranate', 'img/SantalPomegranate.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Green Apple', 'img/SantalGreenApples.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Cherry', 'img/SantalCherry.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Sour Cherry', 'img/SantalSourCherry.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Santal', 150, 0, 'Red Orange', 'img/SantalRedOrange.jpg', 6.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Orange', 'img/PfannerOrange.jpg', 12.9, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Green Apple', 'img/PfannerGreenApples.jpg', 10.7, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Pineapple', 'img/PfannerPineappleCoconut.jpg', 6.5, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Mango', 'img/PfannerMangoApple.jpg', 6.5, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Apricot', 'img/PfannerApricotOrange.jpg', 8.9, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Pfanner', 150, 0, 'Pomegranate', 'img/PfannerPomegranate.jpg', 6.5, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Peach', 'img/GraniniPeach.jpg', 5.2, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Orange', 'img/GraniniOrange.jpg', 4.7, 1.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Blackcurrant', 'img/GraniniBlackcurrant.jpeg', 5.2, 0.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Apple', 'img/GraniniApple.jpg', 5.2, 0.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Sour Cherry', 'img/GraniniSourCherry.jpg', 5.2, 0.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Grapefruit', 'img/GraniniGrapefruit.jpg', 5.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Granini', 150, 0, 'Mango', 'img/GraniniMango.jpg', 5.2, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Delhaize Bio', 150, 0, 'Orange', 'img/GraniniOrangeMango.jpg', 10.5, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Delhaize Bio', 150, 0, 'Beetroot', 'img/GraniniBeetroot.jpg', 10.5, 0.5);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Delhaize Bio', 150, 0, 'Carrot', 'img/GraniniCarrots.jpg', 10.5, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Delhaize Bio', 150, 0, 'Mint', 'img/GraniniMint.jpg', 10.5, 1);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tedi', 150, 0, 'Multifruct', 'img/TediHoney.jpg', 1.2, 0.3);
INSERT INTO product (name, stock, carbo, flavour, img_path, price, volume) VALUES ('Tedi', 150, 0, 'Strawberry', 'img/TediCarrotStraw.jpg', 1.2, 0.5);
INSERT INTO product (name, stock, flavour, img_path, price, volume) VALUES('Gatorade', 200, 'Energizer', 'img/Gatorade.jpg', 5, 0.75); 

COMMIT;