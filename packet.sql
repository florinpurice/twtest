CREATE OR REPLACE PACKAGE myPack AS
	FUNCTION changePass(
						nick IN VARCHAR2, 
						old_password IN VARCHAR2,
						new_password IN VARCHAR2,
						new_password2 IN VARCHAR2
						) RETURN INTEGER;
END myPack;
/
CREATE OR REPLACE PACKAGE BODY myPack AS
	FUNCTION changePass(
						nick IN VARCHAR2, 
						old_password IN VARCHAR2,
						new_password IN VARCHAR2,
						new_password2 IN VARCHAR2
						) RETURN INTEGER IS
	passcheck VARCHAR2(30);
	BEGIN 
		SELECT password INTO passcheck FROM person WHERE username = nick;
		IF passcheck = old_password AND new_password = new_password2 THEN
			UPDATE person SET password = new_password WHERE username = nick;
			RETURN 1;
		ELSE 
			RETURN 0;
		END IF;
	END changePass;
END myPack;
/