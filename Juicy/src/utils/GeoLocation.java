package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class GeoLocation 
{
	private String ip;
	private String country;
	private String city;
	
	public GeoLocation(String ip) throws IOException
	{
		this.setIp(ip);
		getGeoLocation();
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public void getGeoLocation() throws IOException
	{	
		try{
		String linie ;
		URL url = new URL("http://www.telize.com/geoip/"+ip);
		InputStream in = url.openStream ();
		BufferedReader br = new BufferedReader (new InputStreamReader (in));
		while (( linie = br. readLine ()) != null ) {
			if(linie.contains("city"))
			{
				city = linie.substring(linie.indexOf("city")+7, linie.indexOf("\"", linie.indexOf("city")+7));
			}
			
			if(linie.contains("\"country\""))
			{
				country = linie.substring(linie.indexOf("country\"")+10, linie.indexOf("\"", linie.indexOf("country\"")+10));
			}
		}
		}
		catch(IOException e){}
		
	}
	
	
	
}
