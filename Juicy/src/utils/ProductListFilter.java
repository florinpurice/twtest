package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import services.OrderHistoryService;
import models.Order;
import models.Product;
import models.ProductList;
import models.ShoppingListItem;

public class ProductListFilter {
	private Map<String, Integer> mapOfFlavours = null;
	private Product[] filteredList = null;
	
	public ProductListFilter(String[] flavours, String carb, String filterType, String sortBy, String sortOrder,int idUser){
		Map<Integer, Product> map = ProductList.getMap();
		map = filterList(map, filterType,idUser);
		if(carb != null)
			if(carb.equals("yes"))
				map = filterList(map, "carb",idUser);
			else
				map = filterList(map, "nocarb",idUser);
		mapOfFlavours = getFlavoursForList(map.values());
		Product[] list;
		if(flavours == null || flavours.length < 1)
			list = map.values().toArray(new Product[map.size()]);
		else{
			Map<Integer, Product> unionRes = new HashMap<>();
			for(String flavour:flavours)
				unionRes.putAll(flavourFilter(map, flavour));
			list = unionRes.values().toArray(new Product[unionRes.size()]);
		}
		filteredList = sortList(list, sortBy, sortOrder);
	}
	
	public Product[] getList(){
		return filteredList;
	}
	
	public Map<String, Integer> getFlavourMap(){
		return mapOfFlavours;
	}
	
	private Map<String, Integer> getFlavoursForList(Collection<Product> givenList){
		Map<String, Integer> list = new TreeMap<>();
		for(Product product:givenList){
			if(list.containsKey(product.getFlavour()))
				list.replace(product.getFlavour(), list.get(product.getFlavour())+1);
			else
				list.put(product.getFlavour(), 1);
		}
		
		return list;
	}
	
	private Map<Integer, Product> filterList(Map<Integer, Product> map, String filterType,int idUser){
		Stream<Product> str = map.values().stream();
		if(filterType.equals("ieftin"))
			str = str.filter(product -> product.getPrice() < 4);
		else if(filterType.equals("new"))
			str = str.filter(product -> (
					TimeUnit.DAYS.convert((new Date()).getTime()-product.getDateAdded().getTime(), TimeUnit.MILLISECONDS) <= utils.MagicNumbers.newDayLimit
					));
		else if(filterType.equals("carb"))
			str = str.filter(product -> product.isCarbonated());
		else if(filterType.equals("nocarb"))
			str = str.filter(product -> !product.isCarbonated());
		else if(filterType.equals("recomandate"))
			return getRecomandate(idUser);
		else return map;
		
		return str.collect(Collectors.toMap(Product::getId , p->p));
	}
	private Map<Integer, Product> flavourFilter(Map<Integer, Product> map, String flavour){
		return map.values().stream().filter(product -> product.getFlavour().equals(flavour)).collect(Collectors.toMap(Product::getId , p->p));
	}
	private Product[] sortList(Product[] list, String sortBy, String sortOrder){
		if(list == null)
			return list;
		if(sortBy.equals("name")){
			Arrays.sort(list, 
					(p1, p2) -> sortOrder.equals("desc")? p2.getName().toLowerCase().compareTo(p1.getName().toLowerCase()) : p1.getName().toLowerCase().compareTo(p2.getName().toLowerCase()));
		}
		else if(sortBy.equals("price")){
			Arrays.sort(list,
					(p1, p2) -> sortOrder.equals("desc")? Double.compare(p2.getPrice(), p1.getPrice()) : Double.compare(p1.getPrice(), p2.getPrice()));
		}
		else if(sortBy.equals("id")){
			Arrays.sort(list, 
					(p1, p2) -> sortOrder.equals("desc")? Integer.compare(p2.getId(), p1.getId()) : Integer.compare(p1.getId(), p2.getId()));
		}
		else if(sortBy.equals("volume")){
			Arrays.sort(list, 
					(p1, p2) -> sortOrder.equals("desc")? Double.compare(p2.getVolume(), p1.getVolume()) : Double.compare(p1.getVolume(), p2.getVolume()));
		}
		else if(sortBy.equals("stock")){
			Arrays.sort(list, 
					(p1, p2) -> sortOrder.equals("desc")? p2.getStock()-p1.getStock() : p1.getStock()-p2.getStock());
		}
		return list;
	}
	
	private Map<Integer, Product> getRecomandate(int idUser)
	{
		List<Product> listaProduse = new ArrayList<Product>();
		Map<Integer, Product> productMap = new HashMap<Integer, Product>();
		OrderHistoryService orderHistoryService = new OrderHistoryService();
		List<Order> orderList = orderHistoryService.getList(idUser);
		if(orderList == null || orderList.size() == 0)
			return productMap;
		Order order = orderList.get(orderList.size()-1);
		Collection<ShoppingListItem> lastOrder = order.getOrderItems();
		Map<Integer, Product> map = ProductList.getMap();
		if(lastOrder == null)
			return productMap;

		for(ShoppingListItem s:lastOrder)
			for(Product i:map.values())
				if(i.getFlavour().equals(map.get(s.getIdProduct()).getFlavour()) || i.getName().equals(map.get(s.getIdProduct()).getName()))
					listaProduse.add(i);

		for (Product product : listaProduse)
		   productMap.put(product.getId(), product);
		return productMap;		
	}
}
