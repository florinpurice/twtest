package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/clearCart")
public class ClearCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().removeAttribute("cart");
		String redirPage = request.getHeader("referer");
		if(redirPage == null || !redirPage.endsWith(".jsp"))
			redirPage = "index.jsp";
		response.sendRedirect(redirPage);
	}

}
