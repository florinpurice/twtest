package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.OrderPlacer;
import models.Order;
import models.ShoppingCart;
import models.User;

@WebServlet("/order")
public class MakeOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		ShoppingCart cart = (ShoppingCart) request.getSession().getAttribute("cart");
		RequestDispatcher rd = request.getRequestDispatcher("order.jsp");
		if(user == null){
			request.setAttribute("msg", "You need to be logged in to perform this action");
			rd.forward(request, response);
			return;
		}
		if(cart == null || cart.listLength() < 1){
			request.setAttribute("msg", "No items in cart, no order to make.");
			rd.forward(request, response);
			return;
		}
		
		Order order = OrderPlacer.make(user.getId(), cart.getList());
		String msg = null;
		if(order != null){
			int failed = cart.getList().size() - order.getOrderItems().size();
			request.setAttribute("order", order);
			if(failed > 0)
				msg = "Order partially placed. "+failed+" items were not placed due to insufficient stock or modified price.";
			else
				msg = "Order successfully placed!";
			cart.getList().clear();
		}
		else
			msg = "No items were ordered. Reason may be insufficient stock or modified price.";
		request.setAttribute("msg", msg);
		rd.forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
