package controllers;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.ChangePassword;
import services.DatabaseConnection;
import models.User;

@WebServlet("/changepassword")
public class UserChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		String name = user.getUsername();
		String old_password = request.getParameter("old_password");
		String new_password = request.getParameter("new_password");
		String new_password2 = request.getParameter("new_password2");
		String message;
		if(ChangePassword.updatePassword(name, old_password, new_password, new_password2))
			message = "Password has changed succesefully! ";
		else
			message = "Error ! Password didn't change ! ";
		
		request.setAttribute("msg", message);
		request.getRequestDispatcher("messagepage.jsp").forward(request, response);
		
	}
		
		

}