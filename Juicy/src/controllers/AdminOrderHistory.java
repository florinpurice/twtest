package controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.AdminOrder;
import models.User;

@WebServlet("/adminhistory")
public class AdminOrderHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		Map<User, Integer> map = null;
		if(user != null && user.isAdmin()){
			map = new AdminOrder().getUserNumberOfOrdersMap();
		}
		request.setAttribute("userordermap", map);
		request.getRequestDispatcher("fullorderlist.jsp").forward(request, response);
	}
}
