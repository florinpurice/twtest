package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Product;
import models.ProductList;
import models.User;

@WebServlet("/loadproduct")
public class AdminLoadEditProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		Product product = null;
		try{
		if(user != null && user.isAdmin()){
			int id = Integer.parseInt(request.getParameter("id"));
			product = ProductList.getMap().get(id);
		}
		}catch(Exception e){
			product = null;
		}
		request.setAttribute("product",product);
		request.getRequestDispatcher("adminproduct.jsp").forward(request, response);
	}
}
