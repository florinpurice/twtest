package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Order;
import models.User;
import services.AdminOrder;
import services.OrderHistoryService;

@WebServlet("/userorders")
public class AdminUserOrderDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		List<Order> orderList = null;
		
		try{
			if(user != null && user.isAdmin()){
				int idUser = Integer.parseInt(request.getParameter("id"));
				orderList = new OrderHistoryService().getList(idUser);
			}
		}catch(Exception e){
			orderList = null;
		}
		request.setAttribute("orderHistory", orderList);
		request.getRequestDispatcher("orderhistory.jsp").forward(request, response);
	}
}
