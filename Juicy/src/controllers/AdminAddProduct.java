package controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Product;
import models.User;
import services.AdminOrder;
import services.ProductManager;

@WebServlet("/addproduct")
public class AdminAddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		String message = null;
		try{
			if(user != null && user.isAdmin()){
				String name = request.getParameter("name");
				String flavour = request.getParameter("flavour");
				String img_path = request.getParameter("img_path");
				String carboString = request.getParameter("carbo");
				boolean carbo = carboString!=null && carboString.equals("yes");
				String availableString = request.getParameter("available");
				boolean available = availableString!=null && availableString.equals("yes");
				int stock = Integer.parseInt(request.getParameter("stock"));
				double price = Double.parseDouble(request.getParameter("price"));
				double volume = Double.parseDouble(request.getParameter("volume"));
				
				Product product = new Product();
				product.setName(name);
				product.setFlavour(flavour);
				product.setImgPath(img_path);
				product.setCarbonated(carbo);
				product.setStock(stock);
				product.setPrice(price);
				product.setVolume(volume);
				
				int id = new ProductManager().addNewProduct(product, available);
				if(id >= 0)
					message = "Product successfully added. id="+id;
				else
					message = "Product could not be added.";
			}
		}catch(Exception e){
			message = "Incorect inputs.";
		}
		request.setAttribute("msg", message);
		request.getRequestDispatcher("adminproductaddresult.jsp").forward(request, response);
	}
}
