package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.OrderHistoryService;
import models.Order;
import models.User;

@WebServlet("/history")
public class OrderHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		List<Order> orderList = null;
		if(user != null)
			orderList = new OrderHistoryService().getList(user.getId());
		request.setAttribute("orderHistory", orderList);
		request.getRequestDispatcher("orderhistory.jsp").forward(request, response);
	}
}
