package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.UsersManager;
import models.User;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		UsersManager userManager = new UsersManager();
		User user = userManager.getUser(username, password);
		if(user != null){
			request.getSession().setAttribute("user", user);
			response.sendRedirect("index.jsp");
		}
		else{
			request.setAttribute("message", "Login failed");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

}
