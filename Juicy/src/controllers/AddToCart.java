package controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.ShoppingCart;

@WebServlet("/addToCart")
public class AddToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ShoppingCart cart = (ShoppingCart) request.getSession().getAttribute("cart");
		if(cart == null){
			cart = new ShoppingCart();
			request.getSession().setAttribute("cart", cart);
		}
		Map<String, String[]> param = request.getParameterMap();
		for(String key:param.keySet()){
			try{
				int idProduct = Integer.parseInt(key);
				int quantity = Integer.parseInt(param.get(key)[0]);
				if(quantity > 0)
					cart.addItem(idProduct, quantity);
				else
					cart.removeItem(idProduct);
			}
			catch(Exception e){}
		}
		String redirPage = request.getHeader("referer");
		if(redirPage == null || !redirPage.endsWith(".jsp"))
			redirPage = "index.jsp";
		response.sendRedirect(redirPage);
	}

}
