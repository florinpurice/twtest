package models;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

public class Order {
	private int orderId;
	private int userId;
	private Date dateAdded;
	private Collection<ShoppingListItem> orderItems = new LinkedList<>();
	
	public Order(){}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Collection<ShoppingListItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(Collection<ShoppingListItem> orderItems) {
		this.orderItems = orderItems;
	}
}
