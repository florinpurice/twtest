package models;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ShoppingCart  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	Map<Integer, ShoppingListItem> shoppingList = new HashMap<>();
	
	public Collection<ShoppingListItem> getList(){
		return shoppingList.values();
	}
	public void addItem(int idProduct, int quantity){
		if(!ProductList.getMap().containsKey(idProduct))
			return;
		if(quantity > 0){
			ShoppingListItem item = new ShoppingListItem();
			item.setIdProduct(idProduct);
			item.setQuantity(quantity);
			item.setPricePerUnit(ProductList.getMap().get(idProduct).getPrice());
			shoppingList.put(idProduct, item);
		}
		else
			shoppingList.remove(idProduct);
	}
	public void removeItem(int idProduct){
		shoppingList.remove(idProduct);
	}
	public int listLength(){
		return shoppingList.size();
	}
	public boolean isAdded(int idProduct){
		return shoppingList.containsKey(idProduct);
	}
	public ShoppingListItem getListItem(int idProduct){
		return shoppingList.get(idProduct);
	}
}
