package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import services.DatabaseConnection;

public class ProductList {
	private static Map<Integer, Product> products = new HashMap<>();
	
	static{
		ReadProductsFromDB();
	}
	
	public static Product[] getList(){
		return products.values().toArray(new Product[products.size()]);
	}
	
	public static Map<Integer, Product> getMap(){
		return products;
	}
	
	public static void updateItem(int itemId){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM product WHERE id = ?";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setInt(1, itemId);
			ResultSet result = stmt.executeQuery();
			if(result.next()){
				int available = result.getInt("available");
				if(available != 1)
					products.remove(itemId);
				else{
					Product updatedProduct = new Product(result);
					products.put(itemId, updatedProduct);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void update(){
		ReadProductsFromDB();
	}
	
	private static void ReadProductsFromDB(){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM product WHERE available = 1";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			products.clear();
			while(result.next()){
				Product newProduct = new Product(result);
				products.put(newProduct.getId(), newProduct);
			}
		}
		catch(Exception e){
			products.clear();
		}
	}
}
