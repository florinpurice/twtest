package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Product implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String imgPath;
	private int stock;
	private double price;
	private Date dateAdded;
	private boolean isCarbonated;
	private String flavour;
	private double volume;
	
	public Product(){}
	public Product(ResultSet sqlRes) throws SQLException{
		try{
		id = sqlRes.getInt("id");
		name = sqlRes.getString("name");
		imgPath = sqlRes.getString("img_path");
		stock = sqlRes.getInt("stock");
		price = sqlRes.getDouble("price");
		dateAdded = sqlRes.getDate("date_added");
		isCarbonated = sqlRes.getInt("carbo") == 1;
		flavour = sqlRes.getString("flavour");
		volume = sqlRes.getDouble("volume");
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	public boolean isCarbonated() {
		return isCarbonated;
	}
	public void setCarbonated(boolean isCarbonated) {
		this.isCarbonated = isCarbonated;
	}
	public String getFlavour() {
		return flavour;
	}
	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
}
