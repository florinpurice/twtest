package models;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ShoppingListItem  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	private int idProduct;
	private int quantity;
	private double pricePerUnit;
	
	public ShoppingListItem(){}
	public ShoppingListItem(ResultSet sqlRes) throws SQLException{
		idProduct = sqlRes.getInt("product_id");
		quantity = sqlRes.getInt("quantity");
		pricePerUnit = sqlRes.getDouble("price");
	}
	
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPricePerUnit() {
		return pricePerUnit;
	}
	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
}
