package services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class ChangePassword
{
	public ChangePassword(){}
	
	public static boolean updatePassword(String username,String old_password,String new_password,String new_password2)
	{
		try{
		Connection dbConn = null;
		dbConn = DatabaseConnection.getConnection();
		CallableStatement cs = null;
		cs = dbConn.prepareCall("{? = call mypack.changePass(?,?,?,?)}");
		cs.registerOutParameter(1, Types.INTEGER);
		cs.setString(2, username);
		cs.setString(3, old_password);
		cs.setString(4, new_password);
		cs.setString(5, new_password2);
		cs.executeUpdate();
		int ok = cs.getInt(1);
		if(ok == 1)
			return true;
		else return false;
		}catch(SQLException | ClassNotFoundException e){
			e.printStackTrace();
			return false;
		}
		
		
	}
}
