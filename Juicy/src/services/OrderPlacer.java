package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import models.Order;
import models.Product;
import models.ProductList;
import models.ShoppingListItem;

public class OrderPlacer {
	@SuppressWarnings("finally")
	public static Order make(int idUser, Collection<ShoppingListItem> itemList){
		Order order = new Order();
		Connection dbConn = null;
		try {
			dbConn = DatabaseConnection.getConnection();
		} catch (ClassNotFoundException | SQLException e1) {
			return null;
		}
		try {
			int orderId;
			dbConn.setAutoCommit(false);
			
			String sql = "SELECT max(id) FROM main_order";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			ResultSet sqlRes = stmt.executeQuery();
			
			if(sqlRes.next())
				orderId = sqlRes.getInt(1) + 1;
			else
				orderId = 1;
			order.setOrderId(orderId);
			
			sql = "INSERT INTO main_order (id, idperson) VALUES (?, ?)";
			stmt = dbConn.prepareStatement(sql);
			stmt.setInt(1, orderId);
			stmt.setInt(2, idUser);
			stmt.executeUpdate();
			
			for(ShoppingListItem itemToOrder:itemList){
				Product stockProduct = ProductList.getMap().get(itemToOrder.getIdProduct());
				if(stockProduct != null 
						&& itemToOrder.getPricePerUnit() == stockProduct.getPrice() 
						&& itemToOrder.getQuantity() <= stockProduct.getStock()){
					
					sql = "INSERT INTO order_item (order_id, product_id, price, quantity) "
							+ "VALUES (?, ?, ?, ?)";
					stmt = dbConn.prepareStatement(sql);
					stmt.setInt(1, orderId);
					stmt.setInt(2, itemToOrder.getIdProduct());
					stmt.setDouble(3, itemToOrder.getPricePerUnit());
					stmt.setInt(4, itemToOrder.getQuantity());
					stmt.executeUpdate();
					
					order.getOrderItems().add(itemToOrder);
					
					//update product stock
					sql = "UPDATE product SET stock = stock-? "
							+ "WHERE id = ?";
					stmt = dbConn.prepareStatement(sql);
					stmt.setInt(1, itemToOrder.getQuantity());
					stmt.setInt(2, itemToOrder.getIdProduct());
					stmt.executeUpdate();
				}
			}
			
			if(order.getOrderItems().size() > 0){
				dbConn.commit();
				for(ShoppingListItem item:order.getOrderItems())
					ProductList.updateItem(item.getIdProduct());
			}
			else{
				dbConn.rollback();
				order = null;
			}
		} 
		catch (SQLException e) {
			dbConn.rollback();
			order = null;
		}
		finally{
			try {
				dbConn.setAutoCommit(true);
			} catch (SQLException e) {}
			return order;
		}
	}
}
