package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import models.User;

public class AdminOrder {
	public Map<User, Integer> getUserNumberOfOrdersMap(){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "SELECT idperson, count(*) as order_no "
					+ "FROM main_order GROUP BY idperson";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			ResultSet sqlResult = stmt.executeQuery();
			Map<User, Integer> map = new HashMap<>();
			sql = "SELECT * FROM person WHERE id_person=?";
			stmt = dbConn.prepareStatement(sql);
			while(sqlResult.next()){
				stmt.setInt(1, sqlResult.getInt("idperson"));
				ResultSet userResult = stmt.executeQuery();
				if(userResult.next()){
					int admin_rights = userResult.getInt("admin_rights");
					User user = new User(admin_rights == 1);
					user.setUsername(userResult.getString("username"));
					user.setFirstname(userResult.getString("firstname"));
					user.setLastname(userResult.getString("lastname"));
					user.setPhoneNumber(userResult.getString("phone_number"));
					user.setId(userResult.getInt("id_person"));
					user.setBirthday(userResult.getDate("birthday"));
					user.setEmail(userResult.getString("email"));
					
					map.put(user, new Integer(sqlResult.getInt("order_no")));
				}
			}
			return map;
		}
		catch(Exception e){
			return null;
		}
	}
}
