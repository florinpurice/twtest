package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import models.Product;
import models.ProductList;

public class ProductManager {
	public int addNewProduct(Product product, boolean available){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "INSERT INTO product(name, carbo, stock, flavour, img_path, available, price, volume) "
					+ "VALUES(?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, product.getName());
			stmt.setInt(2, product.isCarbonated()?1:0);
			stmt.setInt(3, product.getStock());
			stmt.setString(4, product.getFlavour());
			stmt.setString(5, product.getImgPath());
			stmt.setInt(6, available?1:0);
			stmt.setDouble(7, product.getPrice());
			stmt.setDouble(8, product.getVolume());
			stmt.executeUpdate();
			
			sql = "SELECT id FROM product "
					+ "WHERE name=? AND flavour=?";
			stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, product.getName());
			stmt.setString(2, product.getFlavour());
			ResultSet idResult = stmt.executeQuery();
			if(idResult.next()){
				int id = idResult.getInt("id");
				ProductList.updateItem(id);
				return id;
			}
			else
				return -1;
		}
		catch(Exception e){
			return -1;
		}
	}
	
	public boolean editProduct(Product product, boolean available){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "UPDATE product "
					+ "SET name = ?, carbo = ?, stock = ?, flavour = ?, img_path = ?, available = ?, price = ?, volume = ? "
					+ "WHERE id = ?";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, product.getName());
			stmt.setInt(2, product.isCarbonated()?1:0);
			stmt.setInt(3, product.getStock());
			stmt.setString(4, product.getFlavour());
			stmt.setString(5, product.getImgPath());
			stmt.setInt(6, available?1:0);
			stmt.setDouble(7, product.getPrice());
			stmt.setDouble(8, product.getVolume());
			stmt.setInt(9, product.getId());
			int res = stmt.executeUpdate();
			if(res > 0){
				ProductList.updateItem(product.getId());
				return true;
			}
			else
				return false;
		}
		catch(Exception e){
			return false;
		}
	}
}
