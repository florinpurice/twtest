package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import models.User;

public class UsersManager {
	public boolean addUser(User user, String password){
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "INSERT INTO person (username, password, firstname, lastname, birthday, phone_number, email) "
					+ "VALUES(?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, user.getUsername());
			stmt.setString(2, password);
			stmt.setString(3, user.getFirstname());
			stmt.setString(4, user.getLastname());
			stmt.setDate(5, new java.sql.Date(user.getBirthday().getTime()));
			stmt.setString(6, user.getPhoneNumber());
			stmt.setString(7, user.getEmail());
			stmt.executeUpdate();
			
			sql = "SELECT id_person FROM person "
					+ "WHERE username=?";
			stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, user.getUsername());
			ResultSet idResult = stmt.executeQuery();
			if(!idResult.next())
				return false;
			user.setId(idResult.getInt(1));
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public User getUser(String username, String password){
		if(username == null || password == null || username.equals("") || password.equals(""))
			return null;
		try{
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM person WHERE username = ?";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setString(1, username);
			ResultSet result = stmt.executeQuery();
			if(result.next()){
				String pass = result.getString("password");
				if(pass.equals(password)){
					String firstname = result.getString("firstname");
					String lastname = result.getString("lastname");
					String phoneNumber = result.getString("phone_number");
					int id = result.getInt("id_person");
					Date birthday = result.getDate("birthday");
					int admin_rights = result.getInt("admin_rights");
					String email = result.getString("email");
					User user = new User(admin_rights == 1);
					user.setUsername(username);
					user.setFirstname(firstname);
					user.setLastname(lastname);
					user.setPhoneNumber(phoneNumber);
					user.setId(id);
					user.setBirthday(birthday);
					user.setEmail(email);
					return user;
				}
			}
			return null;
		}
		catch(Exception e){
			return null;
		}
	}
}
