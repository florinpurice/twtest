package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	private Connection connection;
	
	private static DatabaseConnection instance;
	private DatabaseConnection() throws ClassNotFoundException, SQLException{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521/XE", "system", "password");
	}
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		if(instance == null)
				instance = new DatabaseConnection();
		return instance.connection;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if(connection != null){
			connection.close();
			connection = null;
		}
	}
}
