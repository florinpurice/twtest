package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.Order;
import models.ShoppingListItem;

public class OrderHistoryService {
	public List<Order> getList(int idUser){
		try{
			List<Order> orderList = new ArrayList<>();
			Connection dbConn = DatabaseConnection.getConnection();
			String sql = "SELECT * FROM main_order WHERE idperson=?";
			PreparedStatement stmt = dbConn.prepareStatement(sql);
			stmt.setInt(1, idUser);
			ResultSet result = stmt.executeQuery();
			
			sql = "SELECT * FROM order_item WHERE order_id=?";
			PreparedStatement orderItemStmt = dbConn.prepareStatement(sql);
			
			while(result.next()){
				Order order = new Order();
				order.setOrderId(result.getInt("id"));
				order.setUserId(result.getInt("idperson"));
				order.setDateAdded(result.getDate("date_added"));
				orderItemStmt.setInt(1, order.getOrderId());
				ResultSet orderItemRes = orderItemStmt.executeQuery();
				List<ShoppingListItem> orderItems = new ArrayList<>();
				while(orderItemRes.next())
					orderItems.add(new ShoppingListItem(orderItemRes));
				order.setOrderItems(orderItems);
				orderList.add(order);
			}
			return orderList;
		}
		catch(Exception e){
			return null;
		}
	}
}
