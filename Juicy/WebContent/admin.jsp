<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="models.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Admin page</title>
</head>
<%
	User user = (User)session.getAttribute("user");
%>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="adminpage">
	<%if(user == null || !user.isAdmin()){ %>
		<h3>U no admin</h3>
	<%}else{ %>
		<ul>
			<li><a href="adminhistory">Orders history</a></li>
			<li><a href="adminproduct.jsp">Add new product</a></li>
			<li><a href="adminproducteditlist.jsp">Edit existing product</a></li>
		</ul>
	<%} %>
	</div>
</div>
</body>
</html>