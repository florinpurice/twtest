<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Signup</title>
</head>
<body>
	<div id="all">
	<jsp:include page="include/header.jsp"/>
	<form action="signup" method="post">
		<div class="text" id="text">
			<h3>You do not have an account yet?</h3>
		</div>
		<div class="personal data" id="date-personale">
			<div><h4>Personal data</h4></div><br>
			<div class="webform-textfield" id="webform-numele">
				<label for="edit-submitted-numele">
				First name 
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input type="text" id="edit-submitted-numele" name="firstname" size="60" maxlength="25" class="form-text required"><br>
			</div>
			<div class="webform-textfield" id="webform-prenumele">
				<label for="edit-submitted-prenumele">
				Last name 
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input type="text" id="edit-submitted-numele" name="lastname" size="60" maxlength="25" class="form-text required"><br>
			</div><br>
			<div class="webform-date" id="webform-data-nasterii">
				<label for="edit-submitted-data-nasterii">Date of birth 
				</label><br>
				<div class="date">
					<div class="data-nasterii-day">
						  <label class="element-invisible" for="edit-data-nasterii-day">Day </label>
						  <select class="day form-select" id="edit-submitted-data-nasterii-day" name="day">
						  	<option value="" selected="selected">Day</option>
						  	<option value="1">1</option>
						  	<option value="2">2</option>
						  	<option value="3">3</option>
						  	<option value="4">4</option>
						  	<option value="5">5</option>
						  	<option value="6">6</option>
						  	<option value="7">7</option>
						  	<option value="8">8</option>
						  	<option value="9">9</option>
						  	<option value="10">10</option>
						  	<option value="11">11</option>
						  	<option value="12">12</option>
						  	<option value="13">13</option>
						  	<option value="14">14</option>
						  	<option value="15">15</option>
						  	<option value="16">16</option>
						  	<option value="17">17</option>
						  	<option value="18">18</option>
						  	<option value="19">19</option>
						  	<option value="20">20</option>
						  	<option value="21">21</option>
						  	<option value="22">22</option>
						  	<option value="23">23</option>
						  	<option value="24">24</option>
						  	<option value="25">25</option>
						  	<option value="26">26</option>
						  	<option value="27">27</option>
						  	<option value="28">28</option>
						  	<option value="29">29</option>
						  	<option value="30">30</option>
						  	<option value="31">31</option>
						 </select>
					</div>
					<div class="data-nasterii-month">
					  	<label class="element-invisible" for="data-nasterii-month">Month </label>
						<select class="month form-select" id="data-nasterii-month" name="month">
							<option value="" selected="selected">Month</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
					</div>
					<div class="data-nasterii-year">
					  	<label class="element-invisible" for="data-nasterii-year">Year </label>
						<select class="year form-select" id="data-nasterii-year" name="year">
							<option value="" selected="selected">Year</option>
							<option value="1965">1965</option><option value="1966">1966</option><option value="1967">1967</option><option value="1968">1968</option><option value="1969">1969</option>
							<option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option>
							<option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option>
							<option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option>
							<option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option>
							<option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option>
							<option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option>
							<option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option>
						</select>
					</div>
				</div>
			</div><br>
			<div class="number" id="numar-de-telefon">
			  	<label for="telefon">Phone number <span class="form-required" title="Acest c�mp este obligatoriu.">*</span></label><br>
			 	<input type="text" id="edit-submitted-telefon" name="phonenumber" value="" size="60" maxlength="10" class="form-text required">
			</div>
		</div><br><br>
	
		<div class="account data" id="date-cont">
			<div><h4>Account data</h4></div><br>
			<div class="webform-component-textfield" id="webform-component-numele">
				<label for="edit-submitted-nume-cont">
				Username 
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input type="text" id="edit-submitted-nume-cont" name="username" value="" size="60" maxlength="25" class="form-text required"><br>
			</div>
			<div class="email" id="e-mail">
				<label for="e-mail">E-mail 
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input class="email form-text" type="email" id="e-mail" name="e_mail" size="60">
			</div>
			<div class="password" id="parola">
				<label for="parola">Password
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input type="password" name="password" size="60"><br/>
				<label for="parola">Confirm password
				<span class="form-required" title="Acest c�mp este obligatoriu.">*</span>
				</label><br>
				<input type="password" name="passcheck" size="60"><br/>
			</div>	
		</div><br><br>

		<button type="submit" style=" border-radius: 10px; padding-left: 25px; padding-right: 25px; padding-top: 5px; padding-bottom: 5px; background-color: yellow; ">Confirm</button>
	</form>
	</div>
</body>
</html>