<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="loginform">
		<form action="login" method="post">
			<ul>
			<li>Username<input type="text" name="username"/></li>
			<li>Password <input type="password" name="password"/></li>
			</ul><br>
			<button type="submit" style="border-radius: 6px; padding-left: 30px; padding-right: 30px;" >Login</button>
		</form>
		
		<%
		String message = (String)request.getAttribute("message");
		if(message != null){
		%>
		<h1><%=message %></h1>
		<%} %>
	</div>
	</div>
</body>
</html>