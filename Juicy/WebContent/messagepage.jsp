<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Message page</title>
</head>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="fullorder">
	<h3><%=(String)request.getAttribute("msg") %></h3>
	</div>
</div>
</body>
</html>