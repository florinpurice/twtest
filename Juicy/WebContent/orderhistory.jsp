<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ page import="java.util.List, models.Order, models.ShoppingListItem, models.ProductList, 
                 models.Product"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Insert title here</title>
</head>
<%
	List<Order> orderHistory = (List<Order>)request.getAttribute("orderHistory");
%>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
<div id="orderhistory">
<%if(orderHistory == null || orderHistory.size() < 1){ %>
	<h3>No history.</h3>
<%}else{ %>
	<ul>
	<%for(Order order:orderHistory){ %>
		<li>
			OrderId: <%=order.getOrderId() %>, date: <%=order.getDateAdded() %>
			<%
			if(order.getOrderItems() != null && order.getOrderItems().size() > 0){ %>
			<table>
				<tr>
					<th>Product id</th>
					<th>Name</th>
					<th>Price/Unit</th>
					<th>Quantity</th>
				</tr>
				<%for(ShoppingListItem item:order.getOrderItems()){ %>
				<tr>
					<td><%=item.getIdProduct() %></td>
					<%
					Product product = ProductList.getMap().get(item.getIdProduct());
					if(product != null){
					%>
					<td><%=product.getName() %></td>
					<%}else{ %>
					<td>Inexistent product</td>
					<%} %>
					<td><%=item.getPricePerUnit() %></td>
					<td><%=item.getQuantity() %></td>
				</tr>
				<%} %>
			</table>
			<%} %>
		</li>
	<%} %>
	</ul>
<%} %>
</div>
</div>
</body>
</html>