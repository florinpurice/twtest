<%@ page import="models.User, models.ShoppingCart , utils.GeoLocation" %>
<%
User user = (User)session.getAttribute("user");
ShoppingCart cart = (ShoppingCart)session.getAttribute("cart");
//GeoLocation location = new GeoLocation(request.getRemoteAddr());
%>
<div id="header">
	<div id="headerLogo">
		<a href="index.jsp">
			<img src="webimg/juicy.png"/>
		</a>
	</div>
	<div id="rightHeader">
		<div id="auth">
		<%if(user == null){//nelogat %>
			<a href="login.jsp">Login</a>
			| 
			<a href="signup.jsp">Signup</a>

		<%}else{//logat %>
			Hello, <%=user.getFirstname()%>! <!-- Location: <%//=location.getCountry() %>, <%//=location.getCity() %> -->
			|
			<a href="profile.jsp">Profile</a>
			<form action="logout" method="get">
				<button type="submit" style=" border-radius: 10px; ">Logout</button>
			</form>
		<%} %>
		</div>
		<nav id="main">
		<ul id="main" class="navigation">
			<li><div id="admin" style=" color: yellow; ">
				<%if(user != null && user.isAdmin()){ %>
					<a href="admin.jsp" >Admin page</a>
				<%} %>
			</div></li>
			<li>
				<div><a href="help.jsp">Help</a></div>
			<li>
				<div><a href="contact.jsp">Contact </a></div>
			</li>
			<li>
				<div><a href="index.jsp?filter=all&pageNo=1">All products</a></div>
			</li>
			<li>
				<div><a href="index.jsp?filter=new&pageNo=1">New products</a></div>
			</li>
			<li>
				<div><a href="index.jsp">Home</a></div>
			</li>
		</ul>
		</nav>
	</div>

</div>