<%@ page import="models.Product, models.ProductList, utils.MagicNumbers, models.ShoppingCart" %>
<%
Product[] list = (Product[])session.getAttribute("products");
ShoppingCart cart = (ShoppingCart)session.getAttribute("cart");
int pageNumber = Integer.parseInt(request.getParameter("page"));
int pageSize = MagicNumbers.pageSize;
if(list != null && list.length>0){
	int val;
%>
<table>
	<tr>
		<th width="50px">Logo</th>
		<th width="150px">Name</th>
		<th width="150px">Flavour</th>
		<th width="70px">Price</th>
		<th width="50px">Stock</th>
		<th width="70px">Volume</th>
		<th width="40px">Id</th>
	</tr>
	<%for(int i = (pageNumber-1)*pageSize; i < list.length && i < pageNumber*pageSize; ++i){ %>
	<tr>
		<%
		val = cart != null && cart.isAdded(list[i].getId()) ? cart.getListItem(list[i].getId()).getQuantity() : 0;
		%>
		<td class="logocol"><img src="<%=list[i].getImgPath()%>" width="50" height="50"/></td>
		<td><%=list[i].getName() %></td>
		<td><%=list[i].getFlavour() %></td>
		<td><%=list[i].getPrice() %></td>
		<td><%=list[i].getStock() %></td>
		<td><%=list[i].getVolume() %></td>
		<td><%=list[i].getId() %></td>
		<td><input id="<%=list[i].getId() %>" name="<%=list[i].getId() %>" type="number" 
				min="0" max="<%=list[i].getStock()%>" <%if(val > 0 ) {%>value="<%=val%>"<%} %>/>
		</td>
		<td><button type="button" style=" border-radius: 6px;" onclick="addItem(<%=list[i].getId() %>)">Add item</button></td>
	</tr>
	<%} %>
</table>
<%}%>