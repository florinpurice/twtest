<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.User, models.Product" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Admin product</title>
</head>
<%
	User user = (User)session.getAttribute("user");
%>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="adminprod">
	<%if(user == null || !user.isAdmin()){ %>
		<h3>U no admin</h3>
	<%}else{ 
		Product p = (Product)request.getAttribute("product");
	%>
	<form action="<%if(p!=null){ %>editproduct<%}else{ %>addproduct<%} %>" method="POST">
		Name:<br/>
		<input type="text" name="name" <%if(p!=null){ %>value="<%=p.getName()%>"<%} %>/><br/>
		Stock:<br/>
		<input type="number" name="stock" <%if(p!=null){ %>value="<%=p.getStock()%>"<%} %>/><br/>
		Price:<br/>
		<input type="text" name="price" <%if(p!=null){ %>value="<%=p.getPrice()%>"<%} %>/><br/>
		Flavour:<br/>
		<input type="text" name="flavour" <%if(p!=null){ %>value="<%=p.getFlavour()%>"<%} %>/><br/>
		Image path:<br/>
		<input type="text" name="img_path" <%if(p!=null){ %>value="<%=p.getImgPath()%>"<%} %>/><br/>
		Volume:<br/>
		<input type="text" name="volume" <%if(p!=null){ %>value="<%=p.getVolume()%>"<%} %>/><br/>
		<br>
		Carbonated:<br/><br>
		<input type="radio" name="carbo" value="yes" <%if(p==null || p.isCarbonated()){ %>checked<%} %>/>yes<br/>
		<input type="radio" name="carbo" value="no" <%if(p!=null && !p.isCarbonated()){ %>checked<%} %>/>no<br/>
		Available:<br/><br>
		<input type="radio" name="available" value="yes" checked/>yes<br/>
		<input type="radio" name="available" value="no" />no<br/><br><br>
		<%if(p!=null){ %>
		<input type="hidden" name="id" value="<%=p.getId()%>"/>
		<%} %>		
		<button type="submit" style=" border-radius: 10px; padding-left: 25px; padding-right: 25px; padding-top: 5px; padding-bottom: 5px; background-color: yellow; float:right;">send</button>
	</form>
	<%} %>
	</div>
</div>
</body>
</html>