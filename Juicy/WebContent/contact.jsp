<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div id="all">
		<jsp:include page="include/header.jsp"/>
		<div id="contact">
			<h3>S.C. Juicy Distribution S.R.L. </h3> <br>
			<h4>Where you can find us?</h4><br>
			<p>str. TRAIAN, nr. 184-186, et. 5, <br>
			   Bucuresti-sector 2,<br>
			   cod po 24043,<br>
			   BUCURESTI<br>
			</p>
			<h4>E-mail address: juicyDistribution@gmail.com</h4>
			<br><br>
			<h4>For any type of problem you have with your order please call at:</h4>
			 <p>tel/fax: 0265 260 311 <br>
			 <p>mobile:	 0747 010 844 <br>
           	 <p>		 0771 056 534
		</div>
	</div>
</body>
</html>