<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.ShoppingCart, models.ShoppingListItem, models.ProductList, java.util.ArrayList, models.User" %>
<%@ page import="java.util.Map, models.Product" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="javascript/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="javascript/cartlist.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Cart</title>
</head>
<%
ShoppingCart cart = (ShoppingCart)session.getAttribute("cart");
User user = (User)session.getAttribute("user");
%>
<body>
<!-- for javascript cart initialization -->
<div id="jsinit" hidden="hidden">
<%
if(cart != null)
for(ShoppingListItem item:cart.getList())
out.println(item.getIdProduct());
%>
</div>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="cart">
	<%if(cart == null || cart.listLength() < 1){ %>
	<h4>No products in cart!</h4>
	<%}
	else if(user == null){%>
	<h4>You are not logged</h4>
	<%}else{ 
		double total = 0;
	%>
	<div class="shoppingList">
		<table>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Price/Unit</th>
				<th>Qt.</th>
				<th>Subtotal</th>
			</tr>
			<%		
			for(ShoppingListItem item:cart.getList()){ 
				total += item.getPricePerUnit()*item.getQuantity();
			%>
			<tr>
				<td><%=item.getIdProduct() %></td>
				<td><%=ProductList.getMap().get(item.getIdProduct()).getName() %></td>
				<td id="itemPrice<%=item.getIdProduct()%>"><%=item.getPricePerUnit() %></td>
				<td><input type="number" name="<%=item.getIdProduct() %>" 
					min="0" max="<%=ProductList.getMap().get(item.getIdProduct()).getStock() %>" 
					value="<%=item.getQuantity() %>" id="<%=item.getIdProduct() %>"
					onblur="addItem(<%=item.getIdProduct() %>)"/>
				</td>
				<td id="subtotal<%=item.getIdProduct()%>"><%=item.getPricePerUnit()*item.getQuantity() %></td>
			</tr>
			<%} %>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td id="totalSum"><%=total %></td>
			</tr>
		</table>
	</div>
	<div id="button">
		<br/>
		<form action="clearCart">
			<button type="submit" style="border-radius: 6px;">Clear list</button>
		</form>
		<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"><br><br>
			<!-- Identify your business so that you can collect the payments. -->
			<input type="hidden" name="business" value="razvan_andrey94-facilitator@yahoo.com">

			<!-- Specify a Buy Now button. -->
			<input type="hidden" name="cmd" value="_cart">
			<input type="hidden" name="upload" value="1">

			<%
				Map<Integer, Product> map = ProductList.getMap();
				int counter = 0;
				for(ShoppingListItem item:cart.getList()){
					++counter;
			%>
			<input type="hidden" name="item_number_<%=counter %>" value="<%=item.getIdProduct()%>"/>
			<input type="hidden" name="item_name_<%=counter %>" value="<%=map.get(item.getIdProduct()).getName() %>" />
			<input type="hidden" name="amount_<%=counter %>" value="<%=item.getPricePerUnit() %>"/>
			<input id="pay<%=item.getIdProduct() %>" type="hidden" name="quantity_<%=counter %>" value="<%=item.getQuantity() %>" />
			<%} %>
			
			<input type="hidden" name="return" value="http://<%=request.getServerName() %>:8080/Juicy/order">
			<input type="hidden" name="cancel_return" value="http://<%=request.getServerName() %>:8080/Juicy/index.jsp">

			<!-- Display the payment button. -->
			<input type="image" name="submit"
			src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
			alt="PayPal - The safer, easier way to pay online">
			<img alt="" border="0" width="1" height="1"
			src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
		</form>
	</div>
	<%} %>
	</div>
	</div>
</body>
</html>