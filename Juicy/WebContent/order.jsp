<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.Order, models.ShoppingListItem" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order</title>
<link rel="stylesheet" type="text/css" href="css/general.css"/>
</head>
<%
String msg = (String)request.getAttribute("msg");
Order order = (Order)request.getAttribute("order");
%>
<body>
<div id="all">

<%if(msg == null){ %>
<h1>Support Void</h1>
<%}else{ %>
	<jsp:include page="include/header.jsp"/>
	<div id="order">
	<h2><%=msg %></h2>
	<%if(order != null){ %>
	Order details:<br/>
	<br/>Order number: <%=order.getOrderId() %><br/>
	Item list:<br/>
	<table>
		<tr>
			<th>Product id</th>
			<th>Price/Unit</th>
			<th>Quantity</th>
		</tr>
		<%for(ShoppingListItem item:order.getOrderItems()){ %>
		<tr>
			<td><%=item.getIdProduct() %></td>
			<td><%=item.getPricePerUnit() %></td>
			<td><%=item.getQuantity() %></td>
		</tr>
		<%} %>
	</table>
	<%} %>
<%} %>
</div>
</div>
</body>
</html>