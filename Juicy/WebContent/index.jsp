<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.Product, utils.ProductListFilter, models.ShoppingListItem"%>
<%@ page import="models.ProductList, utils.MagicNumbers, models.ShoppingCart, models.User" %>
<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="css/general.css"/>
	<script src="javascript/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="javascript/cartlist.js" type="text/javascript"></script>
	<title>Home</title>
</head>
<%
ShoppingCart cart = (ShoppingCart)session.getAttribute("cart");
String filterType = request.getParameter("filter") != null ? request.getParameter("filter") : (String)session.getAttribute("listingFilter");
String sortBy = request.getParameter("sortBy") != null ? request.getParameter("sortBy") : (String)session.getAttribute("listingSortBy");
String sortOrder = request.getParameter("order") != null ? request.getParameter("order") : (String)session.getAttribute("listingSortOrder");
int pageNumber;
if(request.getParameter("pageNo") == null){
	Integer aux = (Integer)session.getAttribute("listingPageNo");
	pageNumber = aux != null ? aux.intValue() : 0;
}
else
	try{
		pageNumber = Integer.parseInt(request.getParameter("pageNo"));
	}
	catch(Exception e){
		pageNumber = 1;
	}

if(filterType == null)
	filterType = "all";
if(sortBy == null)
	sortBy= "name";
if(sortOrder == null)
	sortOrder = "asc";

session.setAttribute("listingFilter", filterType);
session.setAttribute("listingSortBy", sortBy);
session.setAttribute("listingSortOrder", sortOrder);
session.setAttribute("listingPageNo", pageNumber);

String carb = request.getParameter("carb");
String[] flavours = request.getParameterValues("flavour");
User user = (User)session.getAttribute("user");
int id;
if(user == null)
	id = 1;
else
	id = user.getId();

ProductListFilter productFilter = new ProductListFilter(flavours, carb, filterType, sortBy, sortOrder,id);
Product[] productList = productFilter.getList();
session.setAttribute("products", productList);

int listLength = productList == null ? 0 : productList.length;
int pageSize = MagicNumbers.pageSize;
int maxPageNumber = listLength%pageSize == 0 ? listLength/pageSize : listLength/pageSize+1;
if(pageNumber < 1)
	pageNumber = 1;
if(pageNumber > maxPageNumber)
	pageNumber = maxPageNumber;
%>
<body>
<!-- for javascript cart initialization -->
<div id="jsinit" hidden="hidden">
<%
if(cart != null)
for(ShoppingListItem item:cart.getList())
out.println(item.getIdProduct());
%>
</div>
	
	
<div id="all">	
	<jsp:include page="include/header.jsp"/>
<div id="wrapper">
	
	<h3>You need something refreshing?Now you can order anything you want!</h3>

	<div id="listModifiers">
		<div id="listFilters">
			<ul>
				<li><a href="index.jsp?filter=recomandate&pageNo=1">Recomandation</a></li>
			</ul>
		</div>
	</div>
	<div id="listbody">
		<div class="pageNumberList">
			<ul>
			<%for(int i = 1; i <= maxPageNumber; ++i){ %>
				<li>
					<button type="button" onclick="changeListPage(<%=i %>)"><%=i %></button>
				</li>
			<%} %>
			</ul>
			<br>
		</div>
		<div id="itemlist">
			<jsp:include page="include/productList.jsp">
				<jsp:param value="<%=pageNumber %>" name="page"/>
			</jsp:include>
		</div>
		<br>
		<div class="pageNumberList">
			<ul>
			<%for(int i = 1; i <= maxPageNumber; ++i){ %>
				<li>
					<button type="button" onclick="changeListPage(<%=i %>)"><%=i %></button>
				</li>
			<%} %>
			</ul>
		</div>
			<div id="shopCart" align="right">
			<h2 class="element-invisible">My shopping cart</h2>
			<a href="cart.jsp">
				<img src="webimg/cart.png"/>
			</a>
			<div id="inCartNo" style="position: relative; float: right; padding: 20px;"><%= cart == null ? 0 : cart.listLength() %></div>
		</div>
	</div>
</div>
<div id="sortForm">
			
		<form action="index.jsp">
			<p>Sort by </p>
			<input type="hidden" name="pageNo" value="1"/>
			<select name="sortBy">
				<option value="none">None</option>
				<option value="name" <%if(sortBy.equals("name")){ %>selected<%} %>>Name</option>
				<option value="price" <%if(sortBy.equals("price")){ %>selected<%} %>>Price</option>
				<option value="stock" <%if(sortBy.equals("stock")){ %>selected<%} %>>Stock</option>
				<option value="id" <%if(sortBy.equals("id")){ %>selected<%} %>>Id</option>
				<option value="volume" <%if(sortBy.equals("volume")){ %>selected<%} %>>Volume</option>
			</select>
			<br>
			<p>Choose order</p>
				<input type="radio" name="order" value="asc" <%if(sortOrder.equals("asc")) {%>checked<%} %>/>Ascending<br>
				<input type="radio" name="order" value="desc" <%if(sortOrder.equals("desc")) {%>checked<%} %>/>Descending
			<br><br>
			<p>Flavour </p>
			<%
			Map<String, Integer> flavourList = productFilter.getFlavourMap();
			for(String flavour:flavourList.keySet()){
				boolean isSet = false;
				if(flavours != null)
				for(String f:flavours)
					if(flavour.equals(f)){
						isSet = true;
						break;
					}
			%>
				<input type="checkbox" name="flavour" value="<%=flavour%>" <%if(isSet){ %>checked<%} %>><%=flavour%>(<%=flavourList.get(flavour) %>)<br>
			<%} %>
			<br>
			<p>Type</p>
				<input type="radio" name="carb" value="yes" <%if(carb!=null && carb.equals("yes")){ %>checked<%} %>>carbonated<br>
				<input type="radio" name="carb" value="no" <%if(carb!=null && !carb.equals("yes")){ %>checked<%} %>>non-carbonated<br>
			<br><br>
			<button type="submit">Apply</button>
			<br>
		</form>
	</div>
	<div id="footer">
		<p>Need help?<a href="help.jsp">Click here</a><p>
	
	</div>
</div>
</body>
</html>