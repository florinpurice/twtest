<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div id="help">
	<h1 style="text-align: center;"><strong>Ghid de Utilizare</strong></h1>
	<p><strong>&nbsp;</strong></p>
	<p style="text-align: justify;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>Acest document va oferi un ghid de utilizare cum sa navigati pe site si cum puteti acesa functionalitatile oferite de site.</p>
	<ol>
		<li>
			<h2><strong>Navigare</strong></h2>
		</li>
	</ol>
	<p style="text-align: justify;">Navigarea&nbsp; site-ului se va face cu ajutorului menului din partea de sus a site-ului. Itemele din menu sunt afisate ca link-uri. Pentru a reveni la pagina principala apasati pe linkul Home sau imaginea care contine Juicy.</p>
	<ol start="2">
		<li>
			<h2><strong>Inregistrarea pe site</strong></h2>
		</li>
	</ol>
	<h4>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2.1 Nu aveti un cont pe site</h4>
	<p style="text-align: justify;">In partea de sus-dreapta a site-ului exista un menu cu doua link-uri: Login si Singup. Apasa-ti pe linkul de Singup care va trimite catre un formular de inscriere. Dupa ce ati completat formularul apasa-ti butonul de confirm. Daca inregistrarea este valida atunci site-ul web va va trimite catre pagina principala logat cu noul cont creat. Daca inregistrarea a esuat atunci veti fi nevoit sa introduceti din nou datele in fiecare camp.</p>
	<h4>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2.2 Aveti creat deja un cont pe site</h4>
	<p>In partea de sus-dreapta a site-ului exista un menu cu doua link-uri: Login si Singup . Apasa-ti pe linkul de Login care va trimite catre o nou pagina unde veti introduce numele la cont si parola la cont.</p>
	<p><strong>&nbsp;</strong></p>
	<ol start="3">
		<li>
			<h2><strong>Cosul de cumparaturi</strong></h2>
		</li>
	</ol>
	<p>Pentru a introduce produse in cosul de cumparaturi&nbsp; cauti produsul pe care doriti sa il cumparati , introduceti cantitatea dorita si dupa aceea apasati butonul Add Item. Repetati aceeasi pasi pentru restul produselor.</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Daca doriti sa schimbati cantitatea unui produs din cos, apasati pe imaginea cosului de cumparaturi din partea de jos a site-ului care va va trimite catre cosul dumneavoastra de cumparaturi, iar acolo veti putea schimba cantitea la fiecare produs pe care l-ati introdus in cos. Pentru a elimina toate produsele din cos apasati butonul Clear List.</p>
	<ol start="4">
		<li>
			<h2><strong>Plata facturii</strong></h2>
		</li>
	</ol>
	<p>Dupa ce ati introdus produsele in cosul de cumparaturi, apasati pe imaginea cosului de cumparaturi din partea de jos a site-ului. Aplicatia va va trimite catre cosul dumneavoastra de cumparaturi unde va aparea un buton Buy Now. Apasati pe buton care va trimite la PayPal pentru plata facturii. Dupa ce ati efectuat plata PayPal va va redirectiona inapoi catre site-ul nostrum unde se va confirma comanda. Pentru a vedea un istoric al comenzilor apasati pe link-ul Order History.</p>
	<div id="back">
		<a href="index.jsp">
			<img src="webimg/juicy2.png"/>
		</a>
	</div>
</div>
</body>
</html>