function changeListPage(pgNo){
	var url = "include/productList.jsp?page=" + pgNo;
	$("#itemlist").load(url);
};
function validateItem(name, val){
	var element = document.getElementById(name);
	var min = parseInt(element.min);
	var max = parseInt(element.max);
	if(val < min || val > max)
		return false;
	else
		return true;
};
function findInArray(arr, elm){
	for(var i = 0; i < arr.length; ++i){
		if(arr[i] == elm)
			return i;
	}
	return -1;
};
function updatePaypalInfo(id, qty){
	var element = document.getElementById("pay"+id);
	if(element)
		element.setAttribute("value", qty);
}
function addItem(name){
	var element = document.getElementById(name);
	var val = parseInt(element.value);
	var valid = validateItem(name, val);
	
	if(!valid || val === 0){
		val = 0;
		element.value = "";
	}
	
	updatePaypalInfo(name, val);
	
	$.post("addToCart", name+"="+val);
	
	var index = findInArray(inCart, name);
	if(index === -1){
		if(val > 0)
			inCart.push(name);
	}
	else{
		if(val < 1)
			inCart.splice(index, 1);
	}
	var itemsNo = document.getElementById("inCartNo");
	if(itemsNo)
		itemsNo.innerHTML = inCart.length;
	var subtotal = document.getElementById("subtotal"+name);
	var price = document.getElementById("itemPrice"+name);
	if(subtotal && price){
		subtotal.innerHTML = (price.innerHTML*val).toFixed(2);
		var total = 0;
		for(var i = 0; i < inCart.length; ++i)
			total = total+parseFloat(document.getElementById("subtotal"+inCart[i]).innerHTML);
		document.getElementById("totalSum").innerHTML = total.toFixed(2);
	}
};

$(document).ready(function (){
	var initDiv = document.getElementById("jsinit");
	var fullList = initDiv.innerHTML;
	inCart = fullList.split("\n").filter(Number);
});