<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.User, java.util.Map" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Admin</title>
</head>
<%
	User user = (User)session.getAttribute("user");
%>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="fullorder">
	<%if(user == null || !user.isAdmin()){ %>
		<h3>U no admin</h3>
	<%}else{ 
		Map<User, Integer> map = (Map<User, Integer>)request.getAttribute("userordermap");
		if(map == null || map.size()<1){
	%>
			<h3>No orders have been made.</h3>
		<%}else{ %>
		<ul>
			<%for(User u:map.keySet()){ %>
			<li>User: <%=u.getUsername() %>, number of orders: <%=map.get(u) %> [<a href="userorders?id=<%=u.getId()%>">See history</a>]</li><br/>
			<%} %>
		</ul>
		<%} %>
	<%} %>
	</div>
	</div>
</body>
</html>