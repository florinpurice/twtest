<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="models.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/general.css"/>
<title>Profile</title>
</head>
<%User user = (User)session.getAttribute("user"); %>
<body>
<div id="all">
	<jsp:include page="include/header.jsp"/>
	<div id="profil">
		<form><a href="history">Order history</a></form>
		<form action="changepassword" method="POST">
			Old Password:<br/>
			<input type="password" name="old_password"/><br><br>
			New Password:<br>
			<input type="password" name="new_password"/></br><br>
			New Password Again:<br>
			<input type="password" name="new_password2"/></br>
			<br><br><br>
			<button type="submit" style=" border-radius: 10px; padding-left: 25px; padding-right: 25px; padding-top: 5px; padding-bottom: 5px; background-color: yellow;">send</button>
		</form>
	</div>
	</div>
</body>
</html>